import React, { useState, useEffect } from 'react';
import './App.css';
import MultiPlayer from './components/MultiPlayer';

function App() {
  const [state, setState] = useState([
    "/birds-songs/szencinege.mp3"
  ]);

  useEffect(() => {
    fetch("http://localhost:5000/birds")
    .then(res => res.json())
    .then(
      (result) => {
        let birds = [];
        result.forEach(bird => {
          birds.push(bird.song)
        });
        setState(birds);
      }
    );
  }, [])

  return (
    <div className="App">
      <h1>Birds</h1>
      { state ? <MultiPlayer urls={state.map((item) => item)}></MultiPlayer>:''}
      {/* <MultiPlayer urls={[process.env.PUBLIC_URL + "/birds-songs/szencinege.mp3"]}></MultiPlayer> */}
      {/* <pre>mp3 : {state.map((item) => item + "\n")}</pre> */}
    </div>
  );
}

export default App;
