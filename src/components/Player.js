export default function player({ player, toggle }) {
    return (
        <div>
            <p>Stream URL: {player.url}</p>
            <button onClick={toggle}>{player.playing ? "Pause" : "Play"}</button>
        </div>
    )
}