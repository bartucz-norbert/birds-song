import React, { useState, useEffect } from "react";
import Player from "./Player";

export default function MultiAudio({urls}) {
  const [sources, setSources] = useState(
    urls.map(url => {
      return {
        url,
        audio: new Audio(process.env.PUBLIC_URL + url)
      };
    })
  );

  const [players, setPlayers] = useState(
    urls.map(url => {
      return {
        url,
        playing: false
      };
    })
  );

  const toggle = targetIndex => () => {
    const newPlayers = [...players];
    const currentIndex = players.findIndex(p => p.playing === true);
    if (currentIndex !== -1 && currentIndex !== targetIndex) {
      newPlayers[currentIndex].playing = false;
      newPlayers[targetIndex].playing = true;
    } else if (currentIndex !== -1) {
      newPlayers[targetIndex].playing = false;
    } else {
      newPlayers[targetIndex].playing = true;
    }
    setPlayers(newPlayers);
  };

  useEffect(() => {
    setSources(urls.map(url => {
      return {
        url,
        audio: new Audio(process.env.PUBLIC_URL + url)
      }
    }));
  
    setPlayers(urls.map(url => {
      return {
        url,
        playing: false
      };
    }));

    sources.forEach((source, i) => {
      source.audio.addEventListener("ended", () => {
        const newPlayers = [...players];
        newPlayers[i].playing = false;
        setPlayers(newPlayers);
      });
    });
    return () => {
      sources.forEach((source, i) => {
        source.audio.removeEventListener("ended", () => {
          const newPlayers = [...players];
          newPlayers[i].playing = false;
          setPlayers(newPlayers);
        });
      });
    };

  },[urls]);

  return (
    <div>
      {/* <pre>{sources.map((item) => item.url + '\n')}</pre> */}
      
      {players.map((player, i) => (
        <audio key={i} controls autoPlay={player.playing}>
          <source src={player.url} type="audio/mpeg"/>
        </audio>
      ))}
      {players.map((player, i) => (
        <Player key={i} player={player} toggle={toggle(i)} />
      ))}
    </div>
  )
}      
